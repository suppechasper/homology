function compile()

lapacklib = fullfile(matlabroot, ...
  'extern', 'lib', 'win32', 'microsoft', 'libmwlapack.lib');
blaslib = fullfile(matlabroot, ...
  'extern', 'lib', 'win32', 'microsoft', 'libmwblas.lib');

mex('-v', '-largeArrayDims', 'mph.cpp', blaslib, lapacklib)
