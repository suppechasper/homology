#define FL_INT long

#include "mex.h"  

#include <string.h>
#include "DenseMatrix.h"

#include "IPCATree.h"
#include "GMRATree.h"
#include "GMRANeighborhood.h"

#include "MultiscaleMappingRips.h"

#include "NodeDistance.h"

#include "Linalg.h"



//Expect 4 arguments
//1 - X; Data matrix each column a data point
//2 - maxD: maxmimal dimension of simplicies to add
//3 - eps: threshold for gmra, i.e. relative radius of leaf nodes (radius(leaf) /
//radius(root) < epsilon
//4 - singleScale: set to 0 for multiscale computation otherwise single scale computation
//
//Returns a matrix with 4 rows with columns: birth, death, at which scale teh
//event hapend, dimension of the event
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
  
    using namespace FortranLinalg;

    
    double *data = (double*) mxGetPr(prhs[0]);
    mwSize m = mxGetM(prhs[0]);
    mwSize n = mxGetN(prhs[0]);
    
    int maxD = mxGetScalar(prhs[1]);
    double eps = mxGetScalar(prhs[2]);
    bool single = ((int) mxGetScalar(prhs[3]) ) != 0;

    std::cout << m << " x " << n << std::endl;
    std::cout << maxD << std::endl;
    std::cout << eps << std::endl;
    std::cout << single << std::endl;

    
    DenseMatrix<double> X(m, n, data);
    
    FixedNodeFactory<double> factory(1);
    IPCATree<double> gmra(eps, IPCATree<double>::RELATIVE_NODE_RADIUS,
        &factory, IPCANode<double>::MIDPOINT );
    gmra.construct(X.data(), m, n);


    EuclideanMetric<double> l2;
    CenterNodeDistance<double> dist(&l2);
    gmra.computeRadii(&dist);
    gmra.computeLocalRadii(&dist);
    GenericGMRANeighborhood<double> nh(gmra, &dist);

    MultiscaleMappingRips<double> mrips(X, nh);
    mrips.run(maxD, single);


    std::map<int, DenseMatrix<double> > &dgms = mrips.getDiagrams(); 

    int size = 0;
    for(std::map<int, DenseMatrix<double> >::iterator it = dgms.begin(); it !=
        dgms.end(); ++it){
      size += it->second.N();
    };

    DenseMatrix<double> dgm(size, 4);
    int index = 0;
    for(std::map<int, DenseMatrix<double> >::iterator it = dgms.begin(); it !=
        dgms.end(); ++it){
      DenseMatrix<double> dg = it->second;
      for(unsigned int i=0; i< dg.N(); i++, index++){
        dgm(index, 0) = dg(0, i);
        dgm(index, 1) = dg(1, i);
        dgm(index, 2) = it->first;
        dgm(index, 3) = dg(2, i);
      }
      dg.deallocate();
    }
    
    mxArray *lhs[1];
    lhs[0] = mxCreateNumericMatrix( (mwSize) dgm.M(), (mwSize) dgm.N(), mxDOUBLE_CLASS, mxREAL);
    double *out = (double*) mxGetPr(lhs[0]);
    
    memcpy(out, dgm.data(), dgm.M()*dgm.N()*sizeof(double));

    plhs[0] = lhs[0]; 
    dgm.deallocate();

    return;
}
