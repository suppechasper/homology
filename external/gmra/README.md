# GMRA #
Geomteric Multiresolution Analysis implementation in C++ with an R package front end.

## R Package Installation ##

The package can be installed through CMake by running

*  make R_gmra_install (after running ccmake)

Or through devtools (requires devtools package):

*  devtools::install_github("suppechasper/gmra")

However, the latest version might not be updated at that location.