//author: Samuel Gerber

#ifndef MULTISCALERIPSHARER_H
#define MULTISCALERIPSHARER_H


#include "FiltrationBuilder.h"
#include "MultiscalePersistentHomology.h"
#include "GMRATree.h"
#include "GMRANeighborhood.h"

#include <map>
#include <vector>
#include <utility>
#include <limits>
#include <list>
#include <vector>






template<typename TPrecision>
class MultiscaleRipsHarer{


  public:
  
    typedef typename FiltrationBuilder<TPrecision>::NeighborMap NeighborMap;
    typedef typename FiltrationBuilder<TPrecision>::Neighbors Neighbors;


  private:
    GMRANeighborhood<TPrecision> &nh;

    std::map< int, Filtration > filts;
    std::map< int, TPrecision > alphas;
    std::map< int, std::vector<int> > mappings;




    class SetupRips : public Visitor<TPrecision>{

      public:

        std::set<GMRANode<TPrecision> *> leaves;

        void visit(GMRANode<TPrecision> *node){
          //node->setNodeInfo( new RipsInfo() );
          if(node->getChildren().size() == 0){
            leaves.insert(node);
          }
        };

    };






    void collectNodes(std::set< GMRANode<TPrecision> * > &nodes,
        std::set<GMRANode<TPrecision> *> &pnodes, TPrecision r){

      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        if(getRadius(n) > r){
          pnodes.insert(n);
        }
        else{
          GMRANode<TPrecision> *p = n->getParent();
          if(p!=NULL){
            pnodes.insert( p );
          }
        }
      }

    };





    TPrecision getRadius(GMRANode<TPrecision> *node){
      GMRANode<TPrecision> *p = node->getParent();
      if(p == NULL){
        return std::numeric_limits<TPrecision>::max();
      }
      return p->getRadius();
    };


    TPrecision getMinRadius( std::set< GMRANode<TPrecision> * > &nodes){
      TPrecision r = std::numeric_limits<TPrecision>::max();
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        r = std::min(r, getRadius(n) );
      }
      return r;
    };



    TPrecision getNthRadius( std::set< GMRANode<TPrecision> * > &nodes, int n){
      std::vector<double> radii(nodes.size());
      int index = 0;
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++, index++){
        GMRANode<TPrecision> *n = *it;
        radii[index] =  getRadius(n);
      }
      std::sort( radii.begin(), radii.end() );
      return radii[n];
    };










    void mapVertices(std::map<GMRANode<TPrecision> *, int> &X,
        std::map<GMRANode<TPrecision> *,int> &Y, std::vector<int>
        &mapping, TPrecision r){

      mapping.resize(X.size());

      for(typename std::map<GMRANode<TPrecision> *, int>::iterator it = X.begin(); it !=
          X.end(); ++it){ 

        if(getRadius(it->first) > r){
          mapping[it->second] = Y[it->first];
        }
        else{
          mapping[it->second] = Y[it->first->getParent()];
        }
      }
    };






  public:

    MultiscaleRipsHarer( GMRANeighborhood<TPrecision> &nhood) : nh( nhood ){ 
    };




    void run( int maxD, float radiusPercentile, bool singleScale = false){
      using namespace FortranLinalg;
      filts.clear();
      mappings.clear();
      alphas.clear();

      GMRATree<TPrecision> *gmra = nh.getTree();
      
      //Find all leave nodes
      SetupRips setup;
      gmra->breadthFirstVisitor(&setup);

      std::set<GMRANode<TPrecision> *> current = setup.leaves;

      //Collect centers for this scale
      std::map<GMRANode<TPrecision> *, int> currentIndexes;


      TPrecision prevTime = 0; 
      int scale = 0;
      while( current.size() > 1){
        

        //Compute neighborhood info
        //TPrecision r= *std::max_element(mappingR.begin(), mappingR.end());
        //TPrecision radius = 1.2*getMinRadius(current);
        TPrecision radius = 1.01 * getNthRadius(current, std::min( 1.0 +
              radiusPercentile * current.size(), current.size()-1.0) );
        if(singleScale){
          radius = std::numeric_limits<TPrecision>::max()/3;
        }

        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it){
          (*it)->setStop( true );
        }

        //Compute neighborhood info
        Neighbors N(current.size());
        int index = 0;
        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it, ++index){

          NeighborMap &nl = N[index];

          typename GMRANeighborhood<TPrecision>::NeighborList nList;
          nh.neighbors(*it, 2*radius, nList);

          for(typename GMRANeighborhood<TPrecision>::NeighborListIterator nIt
              =nList.begin(); nIt != nList.end(); ++nIt){
              int nInd = currentIndexes[nIt->second];
              nl[nInd] = nIt->first;
              N[nInd][index] = nIt->first;
          } 
        }

        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it){
          (*it)->setStop( false );
        }



        //Rips at current scale
        FiltrationBuilder<TPrecision> rips;
        int maxSize = 150000;
        rips.run( N, 0, NULL, 2*radius, maxSize, maxD );
        filts[scale] = rips.getFiltration();

        TPrecision ripsTime = rips.getMaxTime();
        TPrecision mapTime =  radius;
        alphas[scale] = ripsTime;

#ifdef VERBOSE
        std::cout << "#Vertices : " << N.size() << std::endl;
        std::cout << "Radius : " << radius << std::endl;
        std::cout << "Filtration size: " << filts[scale].size() << std::endl;
#endif      
      

   
/*

        //Debug
        std::stringstream ss;
        ss << "vertices-scale-" << scale << ".data";
        LinalgIO<TPrecision>::writeMatrix(ss.str(), Xcurrent);


        //Debug
        std::stringstream ss1;
        ss1 << "edges-scale-" << scale << "-ghost.data";
        writeEdges(filts[scale], ss1.str());

        
        //Debug
        std::stringstream ss2;
        ss2 << "edges-scale-" << scale << ".data";
        writeEdges(filts[scale], ss2.str());

*/



        std::set<GMRANode<TPrecision> *> next;
        std::map<GMRANode<TPrecision> *, int> nextIndexes;
        if(!singleScale){
          collectNodes(current, next, mapTime);

          //Collect centers for this scale

          //create mapping from this to next scale
          std::vector<int> &mapping = mappings[scale];
          mapVertices(currentIndexes, nextIndexes, mapping, mapTime);
        }
        else{
          std::vector<int> &mapping = mappings[scale];
          mapping.resize(currentIndexes.size(), 0);
          //mapVertices(currentIndexes, nextIndexes, mapping, mapTime);
        }
        
        //Setup next scale
        current = next;
        currentIndexes = nextIndexes;
        prevTime = ripsTime;

        ++scale;

        if(singleScale){
          break;
        }

      }


    
    };



    std::map<int, Filtration> &getFiltrations(){
      return filts;
    };


    std::map<int, std::vector<int> > &getMappings(){
      return mappings;
    };

    std::map<int, TPrecision > &getAlphas(){
      return alphas;
    };






};

#endif 
