//author: Samuel Gerber

#ifndef RIPS_H
#define RIPS_H


#include <map>
#include <set>
#include <vector>
#include <utility>
#include <limits>
#include <list>

#include "Filtration.h"

#include "DenseMatrix.h"
#include "Linalg.h"
#include "EuclideanMetric.h"



template<typename TPrecision>
class Rips{
  public:
    typedef typename std::map<int, TPrecision> NeighborMap;
    typedef typename NeighborMap::iterator NeighborMapIterator;
    typedef typename std::vector<NeighborMap> Neighbors; 
    

  private:
    //Simplex and adding time
    Filtration filtration;

  public:


    Rips(){ 

    };


    ~Rips(){

    };



    void run(Neighbors &N, TPrecision minTime, TPrecision maxTime,  int maxSize, int maxD){

      filtration.clear();
      //Build rips based on nn graph 
      for(unsigned int i=0; i < N.size(); i++){
        add(i, N, minTime, maxTime, maxD);
      }

      filtration.sort();
      filtration.unique();

      if(filtration.size() > maxSize + N.size()){
        FiltrationIterator fIt  = filtration.begin();
        std::advance(fIt,maxSize+N.size());
        filtration.erase( fIt, filtration.end() );
      }

    };



    Filtration &getFiltration(){
      return filtration;
    };

    TPrecision getMaxTime(){
      return filtration.back().time;
    };




      
 private:


   TPrecision getInsertTime(Simplex &s, Neighbors &NN){
    
     std::list<Simplex> faces = s.getFaces();
     TPrecision time = 0;
     for(std::list<Simplex>::iterator it = faces.begin(); it != faces.end();
           it++){

       Simplex &f = *it;
       std::set<int>::iterator vIt = f.vertices.begin();
       int i1 = *vIt;
       ++vIt;
       int i2 = *vIt;
       NeighborMapIterator nIt = NN[i1].find(i2);

       //simplex is not in nearest neighbor map
       if(nIt == NN[i1].end()){
         return std::numeric_limits<TPrecision>::max();
       }
       TPrecision d = nIt->second;
       if(d > time){
         time = d;
       }
     }  
       
     return time;

   };



   void add(int index, Neighbors &NN, TPrecision minTime, TPrecision maxTime, int maxD){

     Simplex s;
     s.vertices.insert(index);
     NeighborMap &nn = NN[index];
     for(NeighborMapIterator it = nn.begin(); it != nn.end(); ++it){
       s.vertices.insert(it->first);
     } 

     //for(int i = 0; i <= maxD; i++){
       std::list<Simplex> faces = s.getFaces();
       for(std::list<Simplex>::iterator it = faces.begin(); it != faces.end();
           it++){
         Simplex &f = *it;
         TPrecision t = getInsertTime(f, NN);
         if(t <= maxTime){
           if(t < minTime){
             t = minTime;
           }
           filtration.push_back( FiltrationEntry( f, t) );
         }
       }
     //}

   };


};

#endif 

