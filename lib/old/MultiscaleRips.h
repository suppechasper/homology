//author: Samuel Gerber

#ifndef MULTISCALERIPS_H
#define MULTISCALERIPS_H


#include "MappingRips.h"
#include "GMRATree.h"
#include "GMRANeighborhood.h"
#include "EuclideanMetric.h"
#include "LinalgIO.h"

#include <map>
#include <vector>
#include <utility>
#include <limits>
#include <list>
#include <vector>



#include "MappingPersistentHomology.h"




template<typename TPrecision>
class MultiscaleRips{


  public:
  
    typedef typename MappingRips<TPrecision>::IFiltration IFiltration;
    typedef typename MappingRips<TPrecision>::IFiltrationIterator IFiltrationIterator;
    
    typedef typename MappingRips<TPrecision>::Filtration Filtration;
    typedef typename MappingRips<TPrecision>::FiltrationIterator FiltrationIterator;

    typedef typename MappingPersistentHomology<TPrecision>::Event Event;
    typedef typename MappingPersistentHomology<TPrecision>::Events Eventss;
    typedef typename MappingPersistentHomology<TPrecision>::EventsIterator EventsIterator;


    typedef typename MappingRips<TPrecision>::Neighbor Neighbor;
    typedef typename MappingRips<TPrecision>::NeighborList NeighborList;
    typedef typename MappingRips<TPrecision>::Neighbors Neighbors;

  private:

    class SetupRips : public Visitor<TPrecision>{

      public:

        std::set<GMRANode<TPrecision> *> leaves;

        void visit(GMRANode<TPrecision> *node){
          //node->setNodeInfo( new RipsInfo() );
          if(node->getChildren().size() == 0 && !node->isEmpty() ){
            leaves.insert(node);
          }
        };

    };



    DenseMatrix<TPrecision> X;
    GMRATree<TPrecision> &gmra;
    std::map< int, Filtration > filtrations;
    std::map< int, DenseMatrix<TPrecision> > diagrams;



    void collectNodes(std::set< GMRANode<TPrecision> * > &nodes,
        std::set<GMRANode<TPrecision> *> &pnodes, int scale){

      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        if(n->getScale() != scale){
          pnodes.insert(n);
        }
        else{
          GMRANode<TPrecision> *p = n->getParent();
          if(p!=NULL){
            pnodes.insert( p );
          }
        }
      }

    };




    int getMaxScale(std::set< GMRANode<TPrecision> * > &nodes){
      int maxScale = -1;
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        if(n->getScale() > maxScale){
          maxScale = n->getScale();
        }
      }
      return maxScale;
    };




    TPrecision getMaxRadius( std::set< GMRANode<TPrecision> * > &nodes, int scale){
      TPrecision r = 0;
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        if(n->getScale() == scale){
        //  GMRANode<TPrecision> *p = n->getParent();
        //  if(p != NULL){
            r = std::max( r, n->getRadius() );
        //  }
        }
      }
      return r;
    };





    void makeConsistent(IFiltration &ifilt){

      for(IFiltrationIterator it = ifilt.begin(); it != ifilt.end(); ++it){

        Simplex s = it->first;
        TPrecision tMax = it->second;

        if(s.vertices.size() > 1){
          //check maximal time of face addition
          std::list<Simplex> sList = s.getFaces();
          for(std::list<Simplex>::iterator sIt = sList.begin(); sIt !=
              sList.end(); ++sIt){
            
            Simplex &f = *sIt;

            IFiltrationIterator fIt = ifilt.find(f);
            if(fIt != ifilt.end() ){
              const Simplex &f2 = fIt->first;
              if( f2.getScale() <= s.getScale() ){
                if(tMax < fIt->second){
                  tMax = fIt->second;
                }
              }
              else{
                std::cout << "woof" << std::endl;
              }
            }
            else{
              std::cout << "bark" << std::endl;
            }
          }
          
          it->second = tMax;
        }

      }

    };






    void mapVertices(DenseMatrix<TPrecision> &X,
        DenseMatrix<TPrecision> &Y, std::vector<int> &mapping,
        std::vector<TPrecision> &mappingR, std::vector<TPrecision> &prevMappingR){
    
      mapping.resize(X.N());
      mappingR.resize(Y.N(), 0);

      static EuclideanMetric<TPrecision> metric;
      for(int i = 0; i<X.N(); i++){
        int minIndex = -1;
        TPrecision minDist = std::numeric_limits<TPrecision>::max();
        for(int j=0; j<Y.N(); j++){
          TPrecision d = metric.distance(X, i, Y, j);
          if(d < minDist){
            minIndex = j;
            minDist = d;
          }
        }
        mapping[i] = minIndex;
        minDist = minDist;
        if(mappingR[minIndex] < minDist){
          mappingR[minIndex] = minDist + prevMappingR[i];
        }
      };
    };




    DenseMatrix<TPrecision> getCenters(std::set<GMRANode<TPrecision> *> current,
        std::map<GMRANode<TPrecision> *, int> &indexes){
      //Collect centers for this scale
      DenseMatrix<TPrecision> Xs(X.M(), current.size());
      int index= 0;
      for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it !=
          current.end(); ++it, ++index){
        GMRANode<TPrecision> *node = (*it);
        indexes[node] = index;        
        DenseVector<TPrecision> center = node->getCenter();
        Linalg<TPrecision>::SetColumn(Xs, index, center);
      }
      return Xs;
    };



    TPrecision getRadius(GMRAnode<TPrecision> *node, int maxScale){
      TPrecision r = 0;
      if(node->getScale() == maxScale){
        r = node->getParent()->getRadius();
      }
      return r;
    };




  public:

    MultiscaleRips(DenseMatrix<TPrecision> &Xin, GMRATree<TPrecision> &tree) : X(Xin), gmra(tree){ 
    };



    void run( int maxD){
      //Find all leave nodes
      SetupRips setup;
      gmra.breadthFirstVisitor(&setup);

      std::set<GMRANode<TPrecision> *> current = setup.leaves;
      Filtration currentFiltration;

      GenericGMRANeighborhood<TPrecision> nh(gmra);



      int scale = 0;
      DenseMatrix<TPrecision> Xprev(0, 0);


      std::map<GMRANode<TPrecision> *, int> indexes;
      DenseMatrix<TPrecision> Xs = getCenters(current, indexes);
     


      std::vector<int> mapping;
      std::vector<TPrecision> mappingR;
      std::vector<TPrecision> prevMappingR(Xs.N(), 0);
      mapVertices(Xprev, Xs, mapping, mappingR, prevMappingR);
    

      TPrecision prevTime = 0; 
      while( current.size() > 1){



        //Debug
        std::stringstream ss;
        ss << "vertices-scale-" << scale << ".data";
        LinalgIO<TPrecision>::writeMatrix(ss.str(), Xs);


        //Map previous scale filtration to this scale
        IFiltration mapped;
        for( FiltrationIterator it = currentFiltration.begin(); it !=
            currentFiltration.end(); it++){
          Simplex sNew(scale);

          //
          const Simplex &s = it->simplex;
          for(std::set<int>::iterator vIt = s.vertices.begin(); vIt !=
              s.vertices.end(); ++vIt){
            int v = *vIt;
            sNew.vertices.insert( mapping[v] );
          }

          if(sNew.vertices.size() > 0 ){
            IFiltrationIterator it2 = mapped.find(sNew);
            if(it2 == mapped.end()){
              sNew.setScale(s.getScale());
              mapped[sNew] = it->time;
            }
            else{
              const Simplex &s2 = it2->first;
              if(s.getScale() < s2.getScale()){
                mapped.erase(sNew);
                sNew.setScale( s.getScale() );
                mapped[sNew] = it->time;
              }
              else if(s.getScale() == s2.getScale() ){
                it2->second =  std::max(it->time, it2->second);
              }
              //else leave old one
            }
          }
          else{
            std::cout << "should be root" << std::endl;
          }
        }

        //Make sure the order of simplicies is preserved. It can happen that a
        //lower order Simplex has a larger time stamp
        makeConsistent(mapped);

        std::cout << "Mapped size: " << mapped.size() << std::endl;
        
        
        //Debug
        std::stringstream ss1;
        ss1 << "edges-scale-" << scale << "-ghost.data";
        writeEdges(mapped, ss1.str());



        //Setup next scale
        int maxScale = getMaxScale(current);
        std::set< GMRANode<TPrecision> *> next;
        collectNodes( current, next, maxScale );
        Xprev.deallocate();
        Xprev = Linalg<TPrecision>::Copy(Xs);

        //Compute Homology at current scale
        std::map<GMRANode<TPrecision>*, int> nextIndexes;
        Xs = getCenters(next, nextIndexes);
        mapping.clear();
        mappingR.clear();
        
        mapVertices(Xprev, Xs, mapping, mappingR, prevMappingR);
        //prevMappingR = mappingR;



        //Rips at current scale
        //TPrecision maxRadius = *std::max_element(mappingR.begin(), mappingR.end());
        //TPrecision maxRadius = getMaxRadius( current, maxScale );
        
        MappingRips<TPrecision> rips(Xprev);

        //Compute neighborhood info
        Neighbors N(current.size());
        std::vector<TPrecision> radii(current.size());
        int index = 0;
        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it, ++index){
          
          NeighborList &nl = N[index];
          TPrecision r = getRadius(*it, maxScale); 
          radii(index) = r;
          
          //TPrecision r =  maxRadius; //2*mappingR[ mapping[index] ];

          typename GMRANeighborhood<TPrecision>::NeighborList nList;
          nh.neighbors(*it, r, maxScale, nList);
          
          for(typename GMRANeighborhood<TPrecision>::NeighborListIterator nIt
              =nList.begin(); nIt != nList.end(); ++nIt){
            nl.push_back( Neighbor(nIt->first, indexes[nIt->second] ) );
          } 
        }

        //initialize rips with filtration from previous scale
        //TPrecision minRadius = *std::min_element(radii.begin(), radii.end());
        rips.setMappedFiltration( mapped );
        rips.run( N, 2*maxRadius, maxD, scale );

        currentFiltration = rips.getFiltration();
        filtrations[scale] = currentFiltration;

        //Debug
        std::stringstream ss2;
        ss2 << "edges-scale-" << scale << ".data";
        writeEdges( currentFiltration, ss2.str() );

        
        MappingPersistentHomology<TPrecision> homology;
        homology.run( currentFiltration, mapping, prevTime );
        diagrams[scale] = homology.getDiagram(); 
       

        TPrecision maxTime = homology.getMaximalTime(); 
        
        std::cout << "#Vertices : " << Xs.N() << std::endl;
        std::cout << "Filtration size: " << currentFiltration.size() << std::endl;
        std::cout << "Max Radius: " << maxRadius << std::endl << std::endl;
        std::cout << "Max Time: " << maxTime << std::endl << std::endl;

        
        current = next;
        indexes = nextIndexes;
        prevTime = maxTime;
        scale++;
      }

      Xprev.deallocate();
      Xs.deallocate();

    };


    std::map<int, DenseMatrix<TPrecision> > &getDiagrams(){
      return diagrams;
    };


    std::map<int, IFiltration> &getInverseFiltrations(){
      return filtrations;
    };






  private:

    struct Triple{
      Triple(int s, int index1, int index2):scale(s), i1(index1), i2(index2){};
      int scale;
      int i1;
      int i2;
    };


    //Debug method
    void writeEdges(IFiltration &ifilt, std::string filename){
      std::list< Triple  > edges;
      for(IFiltrationIterator it = ifilt.begin(); it != ifilt.end(); ++it){
        const Simplex &s = it->first;
        if(s.vertices.size() == 2){
          int i1 = *s.vertices.begin();
          int i2 = *s.vertices.rbegin();
          Triple edge(s.getScale(), i1, i2);
          edges.push_back(edge);
        }
      }

      DenseMatrix<TPrecision> E(3, edges.size());
      int index = 0;
      for(typename std::list< Triple >::iterator it = edges.begin(); it!=
          edges.end(); ++it, ++index){
        E(0, index) = it->i1;
        E(1, index) = it->i2;
        E(2, index) = it->scale;
      }
      LinalgIO<TPrecision>::writeMatrix(filename, E);

    };


       //Debug method
    void writeEdges(Filtration &filt, std::string filename){
      std::list< Triple  > edges;
      for(FiltrationIterator it = filt.begin(); it != filt.end(); ++it){
        const Simplex &s = it->simplex;
        if(s.vertices.size() == 2){
          int i1 = *s.vertices.begin();
          int i2 = *s.vertices.rbegin();
          Triple edge(s.getScale(), i1, i2);
          edges.push_back(edge);
        }
      }

      DenseMatrix<TPrecision> E(3, edges.size());
      int index = 0;
      for(typename std::list< Triple >::iterator it = edges.begin(); it!=
          edges.end(); ++it, ++index){
        E(0, index) = it->i1;
        E(1, index) = it->i2;
        E(2, index) = it->scale;
      }
      LinalgIO<TPrecision>::writeMatrix(filename, E);

    };


};

#endif 
