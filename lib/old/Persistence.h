//author: Samuel Gerber

#ifndef PERSISTENCE_H
#define PERSISTENCE_H


#include <map>
#include <set>
#include <vector>
#include <utility>
#include <limits>
#include <list>

#include "DenseMatrix.h"
#include "Linalg.h"
#include "Geometry.h"


class Simplex{

public:
  
  std::set<int> vertices;


  bool operator==(const Simplex& other) const{
    return vertices == other.vertices;
  };
  
  bool operator<(const Simplex& other) const{
    return vertices < other.vertices;
  };


  bool operator>(const Simplex& other) const{
    return vertices > other.vertices;
  };

};


template<typename TPrecision>
class Rips{
  public:
    typedef typename std::map<Simplex, TPrecision> Filtration;
    typedef typename Filtration::iterator FiltrationIterator;
  
  
  private:
    //Simplex and adding time
    Filtration filtration;
    Filtration edges;

    //Nearest Neighbors
    DenseMatrix<int> KNN;
    DenseMatrix<TPrecision> KNND;

    //Data
    DenseMatrix<TPrecision> X;
   

    void buildEdgeMap(){
      //Build sorted map of edges
      for(int i = 1; i < KNN.M(); i++){
        for(int j = 0; j < KNN.N(); j++){
          Simplex s;
          s.vertices.insert( i );
          s.vertices.insert( KNN(i, j) );
          edges[ s ] = KNND(i, j);
        }
      }
    };


  public:


    Rips(DenseMatrix<TPrecision> &Xin, DenseVector<TPrecision> &yin,
                DenseMatrix<int> &KNNin, DenseMatrix<TPrecision> &KNNDin) :
         X(Xin), KNN(KNNin), KNND(KNNDin){ 
    
      buildEdgeMap();
    };

 
    Rips(DenseMatrix<TPrecision> &Xin, int knn, bool smooth = false,
        double eps=0.01) : X(Xin){
      if(knn > (int) X.N()){
        knn = X.N();
      }
      KNN = DenseMatrix<int>(knn, X.N());
      KNND = DenseMatrix<TPrecision>(knn, X.N());

      //Compute nearest neighbors
      Geometry<TPrecision>::computeANN(X, KNN, KNND, eps);

      buildEdgeMap();     
    };





    void run(TPrecision alpha){

      for(int i=0; i<KNN.N(); i++){

        //Add edge to filtration
        Simplex s;
        s.vertices.insert( i );

        //Add all simplicies formed from the neighbors of vertex if they are
        //within the nearest neighbor graph and within distance alpha
        bool added = true;
        double t=0;
        for(int k=1; k<KNN.M() && added; k++){
          int v1 = KNN(k, i);
          for(std::set<int>::iterator it = s.vertices.begin(); it !=
              s.vertices.end(); it++){
            int v2 = *it;
            Simplex tmp;
            tmp.vertices.insert(v1); 
            tmp.vertices.insert(v2);

            double t2 = edges[tmp];
            if(t2 == 0 || t2 > alpha){
              added = false;
              break;
            }
            else{
              t = std::max(t, t2);
            } 

          }

          if(added){
            s.vertices.insert( v1 );
            FiltrationIterator it = filtration.find(s);
            if( it == filtration.end() ){
              filtration[s] = t;
            }
          }

        }
      }

    };



   Filtration &getFiltration(){
     return filtration;
   };


   void setFiltration(Filtration &f){
     filtration=f;
   };


};

#endif 

