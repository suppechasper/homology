//author: Samuel Gerber
//
//Matrix reduction For MultiscaleRipsHarer version
//
//
#ifndef MULTISCALEPERSISTENTHOMOLOGY_H
#define MULTISCALEPERSISTENTHOMOLOGY_H


#include <map>
#include <set>
#include <vector>
#include <utility>
#include <limits>
#include <list>
#include <stdlib.h>

#include "Simplex.h"

#include "DenseMatrix.h"
#include "Linalg.h"
#include "FiltrationBuilder.h"

template <typename TPrecision>
class MultiscalePersistentHomology{

    private:



    typedef std::map<Simplex, int > IDMap;
    typedef typename IDMap::iterator IDMapIterator;
    
    class Column{
      public:
        Column(){
          mappedFrom = -1;
          time = 0;
          dimension = 1;
          transfer = false;
        };

        std::list<int> col;
        TPrecision time;
        int dimension;
        int mappedFrom;
        bool transfer;
    };

    typedef std::vector< Column > BoundaryMatrix;
    typedef typename BoundaryMatrix::iterator BoundaryMatrixIterator;
    typedef typename BoundaryMatrix::reverse_iterator BoundaryMatrixRIterator;





  public:


    typedef typename std::map<Simplex, TPrecision> IFiltration;
    typedef typename IFiltration::iterator IFiltrationIterator;
    typedef typename IFiltration::reverse_iterator IFiltrationRIterator;
 

    typedef typename Filtration::iterator FiltrationIterator;
    typedef typename Filtration::reverse_iterator FiltrationRIterator;

    struct Event{
      public:
        Event(){
          birth =-1;
        };

        Event(const TPrecision &b, const TPrecision &d, Column &c, int s, int
            dim) : birth(b),
        death(d), col(c), scale(s), dimension(dim){};
        TPrecision birth;
        TPrecision death;
        Column col;
        int scale;
        int dimension;
    };

    typedef std::map< int, Event > Events;
    typedef typename Events::iterator EventsIterator;
    typedef typename Events::const_iterator EventsCIterator;




  private:




    std::vector< Events > events;
   
    std::vector< IDMap > ids;
    std::vector< BoundaryMatrix > M;
    std::vector< std::vector<int> > low2id;







    std::list<int> getColumn(const Simplex &s, const Simplex &mapped, int scale){
      std::list<int> col;
      
      std::list<Simplex> faces = s.getFaces();
      for(std::list<Simplex>::iterator it = faces.begin(); it != faces.end();
          ++it){
        col.push_back( getID(*it, scale) );
      }
      
      if(scale < ids.size() -1){
        //add mapping id
        if(mapped.vertices.size() == s.vertices.size() ){
          int mID = getID(mapped, scale+1);
          col.push_back( -mID );
        }
      }

      col.sort();

      return col;
    };






    int getLow(const std::list<int> &col){
      if(col.size() == 0){
        return 0;
      }
      return col.back();
    };



    int getID(const Simplex &s, int scale){
      IDMapIterator it = ids[scale].find(s);
      if( it == ids[scale].end() ){
        return 0;
      }
      return it->second;
    };



    Simplex mapsTo(const Simplex &s, const std::vector<int> &mapping){
      Simplex sNew;    
      for(std::set<int>::iterator vIt = s.vertices.begin(); vIt !=
          s.vertices.end(); ++vIt){
        int vIndex = (*vIt);
        sNew.vertices.insert( mapping[vIndex] );
      }
      return sNew;
    };

    


  public:

    MultiscalePersistentHomology(){ 
    };





    //reduce multiscale filtartion with 0 begin the finest scale stored in the
    //map filts. For each scale a correspond mapping from scale i to i+1 and the
    //time the filtration is mapped to the next scale
    void reduce(std::map<int, Filtration> &filts, std::map<int,
        std::vector<int> > &mappings, std::map<int, TPrecision> &alphas, int
        maxD){
      
      clear();

      M.resize( filts.size() );
      ids.resize( filts.size() );
      events.resize( filts.size() );
      low2id.resize( filts.size() );

      //Build boundary and suspension matrix for each scale

      //Sort filtratatiosn and build id maps
      for(typename std::map<int, Filtration>::iterator sit = filts.begin();
          sit != filts.end(); ++sit){

        int scale= sit->first;
        Filtration &filt = sit->second;
        
        low2id[scale].resize( filt.size() + 1, 0 );

        int index = 1;
        for(FiltrationIterator it = filt.begin(); it != filt.end();
            ++it, ++index){
          ids[scale][ it->simplex ] = index;
        }
      }


      //insert columns to each boundary / suspension matrix 
      for(typename std::map<int, Filtration>::reverse_iterator sit = filts.rbegin(); sit !=
          filts.rend(); ++sit){

        int scale= sit->first;
        
        Filtration &filt = sit->second;
        BoundaryMatrix &bm = M[scale];
        bm.resize(filt.size()+1);


        int index = 1;
        for(FiltrationIterator it = filt.begin(); it != filt.end();
            ++it, ++index){
          FiltrationEntry &fe = *it;
          
          Column c;
          c.time = fe.time;
          const Simplex &s = fe.simplex;
          
          Simplex mapped = mapsTo(s, mappings[scale]);

          c.col = getColumn(s, mapped, scale);
          c.dimension = s.vertices.size();

          if( scale != filts.size()-1 ){
            int mapIndex = getID(mapped, scale + 1 );
            Column &cMapped = M[scale+1][ mapIndex ];
            cMapped.mappedFrom = index;
          }
          bm[index] = c;
        }
      }

      //Reduce each scale and suspension
      for(int i = M.size() - 1; i >= 0; i--){
        TPrecision b0 = 0;
        if(i != 0){
          b0 = alphas[i-1];
        }
        if(i == M.size() - 1){
          BoundaryMatrix empty;
          reduce( empty,  M[i], i, b0, alphas[i], maxD );
        }
        else{
          reduce( M[i+1],  M[i], i, b0, alphas[i], maxD );
        }
      }

      //clean up events - remove within scale events
      TPrecision prevAlpha = 0;
      for(unsigned int i=0; i<events.size(); i++){
        TPrecision alpha = alphas[i];
        TPrecision delta = std::max( alpha - prevAlpha, 0.0 );
        for(EventsIterator it = events[i].begin(); it !=events[i].end();){
          Event &e = it->second;
          if( (e.death - e.birth) <= delta){
            events[i].erase(it++);
          }
          else{
            ++it;
          }
        }
        prevAlpha = alpha;
      } 
    
    };


    void clear(){
      M.clear();
      ids.clear();
      low2id.clear();
      events.clear();
    };




    FortranLinalg::DenseMatrix<TPrecision> getDiagram() const{
      int size = 0;
      for(typename std::vector< Events >::const_iterator it = events.begin(); it != events.end();
          ++it){
        size += (*it).size();
      }
      FortranLinalg::DenseMatrix<TPrecision> ph(4, size);
      
      int index = 0;
      
      for(unsigned int s=0; s < events.size(); s++){

        for(EventsCIterator it = events[s].begin(); it != events[s].end(); ++it, ++index){
          const Event &e = it->second;
          ph(0, index) =  e.birth;
          ph(1, index) =  e.death;
          ph(2, index) =  e.scale;
          ph(3, index) =  e.dimension;
        }
      }
      return ph;
    };



    FortranLinalg::DenseMatrix<TPrecision> getClassMappings() const{
      int size = 0;
      for(typename std::vector< Events >::const_iterator it = events.begin(); it != events.end();
          ++it){
        size += (*it).size();
      }
      FortranLinalg::DenseMatrix<TPrecision> ph(ids.size(), size);
      FortranLinalg::Linalg<TPrecision>::Zero(ph, 0);
       
      int index = 0;
      
      for(unsigned int s=0; s < events.size(); s++){

        for(EventsCIterator it = events[s].begin(); it != events[s].end(); ++it, ++index){
          const Event &e = it->second;
          Column col = e.column;
          
          phi(col.scale, index) = 1;
          while(col.mappedFrom != -1){
            col = M[col.scale+1][col.mappedFrom];
            phi(col.scale, index) = 1;
          }
        }
      }
      return ph;
    };


    const std::vector<BoundaryMatrix> &getMultiscaleBoundaryMatrix() const{
      return M;
    };





  private:

    void reduce(BoundaryMatrix &Mprev, BoundaryMatrix &M2, int scale, TPrecision
        b0, TPrecision b1, int maxD){

      //negate entries of previous scale to distinguish between transfers and
      //within scale entries
      BoundaryMatrix M1;
      negateEntries(Mprev, M1);

      std::vector<int> low2idT(Mprev.size(), 0);

      std::map<int, Column> zeros;
      std::map<int, Column > transfers;
      for(unsigned int i = 0;  i < M2.size(); i++){


        Column &current = M2[i];

        int low = getLow( current.col );
        bool collision = low > 0;
        while( collision ){
          std::list<int> *colp;
          if(low  < 0 ){
            colp = &M1[ low2id[scale+1][-low] ].col;
          }
          else{
            colp = &M2[ low2id[scale][low] ].col;
          }


          std::list<int> diff;
          symmetric(current.col, *colp, diff);
          current.col = diff;
          int newLow = getLow( current.col );

          collision = newLow != low && newLow > 0;
          low = newLow;
        }


        //died in suspensions but should be mapped to avoid duplicate at
        //coarser scaleo
        /*if(low == 0  && prevLow < 0){
          current.col.push_front(prevLow);
          low = prevLow;
        }*/


        if(current.dimension > 1) { 
          if( low < 0 ){
            low2idT[-low] = i;
            if(current.dimension <= maxD){
              transfers[i] = current;
            }
          }
          else if(low > 0 ){
            low2id[scale][low] = i;
            
            zeros.erase(low);
            transfers.erase(low);

            TPrecision birth = M2[low].time;
            if( current.mappedFrom == -1 ){
              events[scale][low] = Event( birth , current.time, current, scale,
                  current.dimension );
            }else{
              events[scale][low] = Event( current.time, current.time, current,
                  scale, current.dimension );
            }
          }
          else{
            if(current.dimension <= maxD){
              zeros[i] = current;
            }
          }
        }
      }



      //updated classes that are transfered
      for(typename std::map<int, Column >::iterator it =
          transfers.begin(); it != transfers.end(); ++it){

      
        Column &current = M[scale][it->first];
        current.transfer = true;

        //find zero column at previous scale
        std::pair<int, int> t = transfer(scale, it->first);
        //std::cout << "transfer: " << t.first << " from " << scale << std::endl;

        EventsIterator eIt = events[t.first].find(t.second);
        if(eIt != events[t.first].end()){
          //if(current.time > b0){
            eIt->second.birth = current.time;
          //}
          //else{
            //eIt->second.birth = b0;
          //}
        }
        else{
#ifdef VERBOSE
          std::cout << "oh oh" << std::endl;
          std::cout << t.first << ", " << t.second << std::endl;
          std::cout << getLow( M[t.first][t.second].col ) << std::endl;
#endif
        }
      }



      //add classes that died in mapping 
      for(typename std::map<int, Column>::iterator it = zeros.begin(); it !=
          zeros.end(); ++it){
        Column &col = it->second;
        if(col.mappedFrom == -1 ){
          events[scale][it->first] = Event(col.time, b1, col, scale,
              col.dimension+1);
        }
        else{
          events[scale][it->first] = Event(b0, b1, col, scale, col.dimension+1);
        }
      }
      
    };





    //compute transfer destination
    std::pair<int, int> transfer(int scale, int cIndex){
      Column &col = M[scale][cIndex];
      if(col.transfer){
        int low = getLow( col.col );
        return transfer(scale+1, -low);
      }
      else{
        std::pair<int, int> tMap(scale, cIndex);
        return tMap;
      }
    };






    void negateEntries(BoundaryMatrix &orig, BoundaryMatrix &neg){
       neg.resize(orig.size());
       for(unsigned int i=0; i<orig.size(); i++){
         Column &co = orig[i]; 
         Column &cn = neg[i];
         cn.time = co.time;
         cn.dimension = co.dimension;
         for(std::list<int>::iterator it = co.col.begin(); it !=
             co.col.end(); ++it){
           cn.col.push_back( -(*it) );
         }
       }

    };



    //symmetric difference treating negative integers differently than positive
    //expects negative integers to be ordered in decreasing order before positive
    //integers ordered in inreasing order
    void symmetric(std::list<int> &l1, std::list<int> &l2, std::list<int>
        &res){
      std::list<int>::iterator it1 = l1.begin();
      std::list<int>::iterator it2 = l2.begin();
      while(it1 != l1.end() && it2 != l2.end() ){
        int i1 = *it1;
        int i2 = *it2;
        if(i1 == i2){
          if(i1 < 0){
            res.push_back(i1);
          }
          ++it1;
          ++it2;
        }
        else if(i1 < 0){
          if(i2 < 0){
            if(i1 < i2){
              res.push_back(i2);
              ++it2;
            }
            else{
              res.push_back(i1);
              ++it1;
            }
          }
          else{
            res.push_back(i1);
            ++it1;
          }
        }
        else if(i2 < 0){
          res.push_back(i2);
          ++it2;
        }
        else{
          if(i1 < i2){
            res.push_back(i1);
            ++it1;
          }
          else{
            res.push_back(i2);
            ++it2;
          }
        }
      }
      while(it1 != l1.end()){
        res.push_back(*it1);
        ++it1;
      }
      while(it2 != l2.end()){
        res.push_back(*it2);
        ++it2;
      }

    };

};

#endif 

