//author: Samuel Gerber


#ifndef CONEMAPRIPS_H
#define CONEMAPRIPS_H


#include "ConeMapFiltrationBuilder.h"
#include "GMRATree.h"
#include "GMRANeighborhood.h"
#include "EigenEuclideanMetric.h"

#include <map>
#include <vector>
#include <utility>
#include <limits>
#include <list>
#include <vector>

#include <Eigen/Dense>

#include "RelativeGMRANeighborhood.h"
#include "ConeMapHomology.h"

template <typename TPrecision>
class GMRAIDNode : public GMRANodeDecorator<TPrecision>{

  private:
    int id;

  public:
    GMRAIDNode(GMRANode<TPrecision> *n, int nodeID-1) : GMRANodeDecorator(n), id(nodeID) {
    };

    void setID(int nodeID){
      id=nodeID;
    };

    int getID(){
      return id;
    }
};



template <typename TPrecision>
class GMRAIDDecorator : public GMRADecorator<TPrecision>{

  private:
    int idCounter;
  public:
    GMRAIDDecorator() : idCounter(0) {
    };

    void resetCounter(){
      idCounter=0;
    };

    virtual GMRANode<TPrecision> *decorate(GMRANode<TPrecision> *node, GMRANode<TPrecision> *parent){

      GMRANode<TPrecision> *dNode = new GMRAIDNode<TPrecision>(node, idCounter);
      ++idCounter;
      return dNode;
    };

};


template<typename TPrecision>
class ConeMapRips{


  public:
    typedef typename Eigen::Matrix<TPrecision, Eigen::Dynamic, Eigen::Dynamic> MatrixXp;
    typedef typename Eigen::Matrix<TPrecision, Eigen::Dynamic, 1> VectorXp;


    typedef typename ConeMapHomology<TPrecision>::Event Event;
    typedef typename ConeMapHomology<TPrecision>::Events Events;
    typedef typename ConeMapHomology<TPrecision>::EventsIterator EventsIterator;

    typedef typename ConeMapHomology<TPrecision>::FiltrationEntryMap
      FiltrationEntryMap;
    typedef typename ConeMapHomology<TPrecision>::FiltrationEntryMapIterator
      FiltrationEntryMapIterator;

    typedef typename ConeMapFiltrationBuilder<TPrecision>::NeighborMap NeighborMap;
    typedef typename ConeMapFiltrationBuilder<TPrecision>::Neighbors Neighbors;




  private:
    GMRANeighborhood<TPrecision> &nh;





  public:

    ConeMapRips( GMRANeighborhood<TPrecision> &nhood):nh(nhood) {
    };



    MatrixXp run( int maxD,
                  float radiusPercentile,
                  bool singleScale = false,
                  float radiusFactor = 1.01 ){

      if( radiusFactor <= 1){
        radiusFactor = 1.01;
      }
      //-- Instance to compute persistence homology at each scale  --//
      ConeMapHomology<TPrecision> homology;


      //-- Setup --//
      if(radiusPercentile < 0 ){
        radiusPercentile = 0;
      }

      //Find all leave nodes
      SetupRips setup;
      GMRATree<TPrecision> *gmra = nh.getTree();
      gmra->breadthFirstVisitor( &setup );

      GMRAIDDecorator<TPrecision> ider;
      gmra->decorate(ider);

      NodeDistance<double> *dist =
         new CenterNodeDistance<double>( new EuclideanMetric<double>() );
      gmra->computeRadii(dist);
      delete dist;

      std::set< GMRANode<TPrecision> * > current = setup.leaves;
      std::map< GMRANode<TPrecision> *, int > currentIndexes;
      getCenters(current, currentIndexes);

      std::vector<int> mapping;
      IMappingFiltration mapped;

      TPrecision prevTime = 0;
      int scale = 0;
      int maxScale = 0;
      for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it){
          maxScale = std::max(maxScale, (*it)->getScale() );
      }


      //-- Run filtration at each scale --//

      //Birth simplicies to propagate across scale
      int nNodes = 0;
      int nNodesPrev = 0;
      while( current.size() > 1){


#ifdef VERBOSE
        std::cout << std::endl << "Computing Rips at scale: " << scale << std::endl;
        std::cout << std::endl << "Computing Rips at scale: " << maxScale << std::endl;
        std::cout << "#Vertices : " << current.size() << std::endl;
        clock_t t1 = clock();
#endif

        //-- Build filtration at current scale

        //TPrecision radius = getNthRadius(current, std::min(current.size()/2.0,
        //     10.0) );
        //TPrecision radius = 1.2 * getMinRadius(current);
        TPrecision radius = 0;
        //TPrecision moveRadius = 1.5 * getMinRelativeRadius(current);
        //radius = std::max(moveRadius, radius);

        //TPrecision epsilon = radius;

        if( singleScale ){
          radius = std::numeric_limits<TPrecision>::max() / 3;
        }else{
          //radius = getScaleMaxRadius(current, maxScale);
          //radius = 1.5 * getMinRadius(current);
          radius = radiusFactor * getNthRadius(current, std::min( 1.0 +
                radiusPercentile * current.size(), current.size()-1.0) );
        }
#ifdef VERBOSE
        std::cout << "Radius Percentile: " << radiusPercentile << std::endl;
        std::cout << "Radius: " << radius << std::endl;
#endif

        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it){
          (*it)->setStop( true );
        }

        //-- Compute neighborhood info
        Neighbors N(current.size());
        int index = 0;
        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it, ++index){

          NeighborMap &nl = N[index];

          typename GMRANeighborhood<TPrecision>::NeighborList nList;
          nh.neighbors(*it, 2*radius, nList);

          for(typename GMRANeighborhood<TPrecision>::NeighborListIterator nIt
              =nList.begin(); nIt != nList.end(); ++nIt){
              int nInd = currentIndexes[nIt->second];
              nl[nInd] = nIt->first;
              N[nInd][index] = nIt->first;
          }
        }

        for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it
            != current.end(); ++it){
          (*it)->setStop( false );
        }



#ifdef VERBOSE
        clock_t t2 =  clock();
        std::cout << "Find neighboors time: " << ((double)t2 - t1) / CLOCKS_PER_SEC;
        std::cout << std::endl;
#endif
        int nNeighbors = 0;
        for(unsigned int i=0; i<N.size(); i++){
          nNeighbors += N[i].size();
        }

#ifdef VERBOSE
        std::cout << "Average #neighbors: " << nNeighbors / (double) N.size();
        std::cout << std::endl;
#endif


        //-- Build filtration
        //Construct weighted filtration using the mapped filtration from the
        //previous scale
        ConeMapFiltrationBuilder<TPrecision> rips;
        rips.setMappedMappingFiltration(mapped);
        int maxSize = 100000000;
        rips.run( N, prevTime, 2*radius, maxSize, maxD, scale, nNodes);
        MappingFiltration filt = rips.getMappingFiltration();
        nNodesPrev = nNodes;
        nNodes += current.size();
        std::cout << "#Nodes: " << nNodes << std::endl;


#ifdef VERBOSE
        clock_t t3 =  clock();
        std::cout << "Rips time: " << ((double)t3 - t2) / CLOCKS_PER_SEC << std::endl;
#endif




        //-- Compute homology at current scale

        TPrecision mapTime = radius;
        if( rips.isTruncated() ){
          TPrecision ripsTime = rips.getMaxTime();
#ifdef VERBOSE
          std::cout << "Rips max length: " << ripsTime << std::endl;
#endif
          mapTime = std::min( ripsTime/2, radius );
        }

        //Modified matrix reduction with mapping filtration
        homology.run( filt, 2.0*mapTime, maxD, scale );
        TPrecision maxTime = homology.getMaximalTime();

#ifdef VERBOSE
        clock_t t4 =  clock();
        std::cout << "Reduction time: " << ((double)t4 - t3) /CLOCKS_PER_SEC;
        std::cout << std::endl;
        std::cout << "Filtration size: " << filt.size();
        std::cout << std::endl;
        std::cout << "Max Time: " << maxTime;
        std::cout << std::endl << std::endl;
#endif




        //-- Construct map to next scale --//
        std::set< GMRANode<TPrecision> * > next;
        std::map< GMRANode<TPrecision> *, int > nextIndexes;

        NeighborMap coneNeighbors;
        //Compute vertex mapping
        IMappingFiltration cone;
        if( !singleScale ){

          collectNodes( current, next, mapTime );
          getCenters( next, nextIndexes );
          mapVertices( currentIndexes, nextIndexes, mapping, mapTime );

          //Map filtration to next scale scale
          mapped.clear();
          mapMappingFiltration( filt, mapping, mapped, cone,  scale,
                                nNodesPrev, nNodes, mapTime, maxD );
        }
        else{
          mapTime = std::numeric_limits<TPrecision>::max()/3;
        }


#ifdef VERBOSE
        clock_t t5 =  clock();
        std::cout << "Mapped filtration size: " << mapped.size() << std::endl;
        std::cout << "Cone mapping filtration size: " << cone.size() << std::endl;
        std::cout << "Compute mapping time: " << ((double)t5 - t4) /CLOCKS_PER_SEC;
        std::cout << std::endl;
#endif



        //Compute cone map
        if( !singleScale ){

          homology.run( cone, mapTime, maxD, scale );

#ifdef VERBOSE
          clock_t t6 =  clock();
          std::cout << "Cone map homology reduction  time: ";
          std::cout << ((double)t6 - t5) / CLOCKS_PER_SEC;
          std::cout << std::endl;
#endif
        }




        //-- Setup for next scale
        if(current.size() == next.size() ){
          radiusPercentile += 0.5 * (1-radiusPercentile);
          radiusFactor *= 1.5;
        }
        current = next;
        currentIndexes = nextIndexes;
        prevTime = maxTime;
        //prevEpsilon = epsilon;
        scale++;
        maxScale--;

        if(singleScale){
          break;
        }

      }

      return homology.getDiagram();
    };



  private:


    class SetupRips : public Visitor<TPrecision>{

      public:

        std::set<GMRANode<TPrecision> *> leaves;

        void visit(GMRANode<TPrecision> *node){
          //node->setNodeInfo( new RipsInfo() );
          if(node->getChildren().size() == 0){
            leaves.insert(node);
          }
        };

    };







    void collectNodes( std::set< GMRANode<TPrecision> * > &nodes,
                       std::set<GMRANode<TPrecision> *> &pnodes,
                       TPrecision r ){

      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        pnodes.insert( getMappedNode(*it, r) );
      }

    };




    GMRANode<TPrecision> *getMappedNode( GMRANode<TPrecision> *node,
        TPrecision r){

      GMRANode<TPrecision> *current = node;
      while( getRadius(current) <= r ){
      //if( getRadius(current) <= r ){
        GMRANode<TPrecision> *parent = current->getParent();
        if(parent != NULL){
          current = parent;
        }
        else{
          break;
        }
      }

      return current;
    };


    TPrecision getRadius(GMRANode<TPrecision> *node){
      return getParentRadius(node);


      TPrecision r = 0;
      GMRANode<TPrecision> *p = node->getParent();
      if(p == NULL){
        p = node;
      }

      std::vector< GMRANode<TPrecision>* > &kids = p->getChildren();

      NodeDistance<TPrecision> *dist = nh.getNodeDistance();
      for(unsigned int i=0; i<kids.size(); i++){
        r = std::max(r, dist->distance(kids[i], p) );
      }

      return r;
      //std::max( r, getRelativeRadius(node) );

    };




    TPrecision getParentRadius(GMRANode<TPrecision> *node){
      GMRANode<TPrecision> *p = node->getParent();
      if(p == NULL){
        return node->getRadius();
      }
      return p->getRadius();
    };


    TPrecision getMinRadius( std::set< GMRANode<TPrecision> * > &nodes){
      TPrecision radius = std::numeric_limits<TPrecision>::max();
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++){
        GMRANode<TPrecision> *n = *it;
        radius = std::min(getRadius(n), radius);
      }
      return radius;
    };

    TPrecision getNthRadius( std::set< GMRANode<TPrecision> * > &nodes, int n){
      std::vector<double> radii(nodes.size());
      int index = 0;
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++, index++){
        GMRANode<TPrecision> *n = *it;
        radii[index] =  getRadius(n);
      }
      std::sort( radii.begin(), radii.end() );
      return radii[n];
    };


    TPrecision getScaleMaxRadius( std::set< GMRANode<TPrecision> * > &nodes, int scale){
      TPrecision radius = 0;
      for(typename std::set< GMRANode<TPrecision> * >::iterator it = nodes.begin(); it!=
          nodes.end(); it++ ){
        GMRANode<TPrecision> *n = *it;
        if(n->getScale() == scale){
          radius = std::max(radius, getRadius(n) );
        }
      }
      return radius;
    };



    void mapVertices( std::map< GMRANode<TPrecision> *, int > &X,
                      std::map< GMRANode<TPrecision> *, int > &Y,
                      std::vector<int> &mapping, TPrecision r  ){

      mapping.clear();
      mapping.resize( X.size() );

      for(typename std::map<GMRANode<TPrecision> *, int>::iterator it = X.begin(); it !=
          X.end(); ++it){
        mapping[it->second] = Y[ getMappedNode(it->first, r) ];
      }

    };


    void getCenters(std::set<GMRANode<TPrecision> *> current,
        std::map<GMRANode<TPrecision> *, int> &indexes){
      int index= 0;
      for(typename std::set<GMRANode<TPrecision> *>::iterator it = current.begin(); it !=
          current.end(); ++it, ++index){
        GMRANode<TPrecision> *node = (*it);
        indexes[node] = index;
      }
    };



    //Map filtration according to mapping
    void mapMappingFiltration( MappingFiltration &filt, std::vector<int> &mapping,
              IMappingFiltration &mapped, IMappingFiltration &cone, int &scale,
              int &nNodesPrev, int &nNodes, double &mapTime, int &maxDim ){


      for( MappingFiltrationIterator it = filt.begin(); it != filt.end(); it++){

        //
        MappingSimplex &s = it->simplex;
        MappingSimplex sNew(scale);
        for(std::set<int>::iterator vIt = s.vertices.begin(); vIt !=
            s.vertices.end(); ++vIt){
          int v = *vIt - nNodesPrev;
          sNew.vertices.insert( mapping[v] + nNodes );
        }

        if( sNew.vertices.size() > 0 ){
          sNew.setScale( scale );
          //IMappingFiltrationIterator mit = mapped.find(sNew);
          //if( mit == mapped.end() ){
            mapped[sNew] = mapTime;
            cone[sNew] = mapTime;
            long nVertices = s.vertices.size();
            if( nVertices < maxDim + 2 ){
              //Add cone simplicies
              if(sNew.vertices.size() == 1){
                MappingSimplex sCone = s;
                sCone.setMapped( false );
                sCone.vertices.insert( *sNew.vertices.begin() );
                cone[sCone] = mapTime;
              }
              else if(sNew.vertices.size() == nVertices){
                //TODO: not sure about that if clause above
                addCones( sNew, s, cone, scale, mapTime, maxDim );
              }
            }
          //}
        }
        else{
#ifdef VERBOSE
          std::cout << "Huuuummmmm" << std::endl;
#endif
        }


      }

#ifdef VERBOSE
      std::cout << "Mapped size: " << mapped.size() << std::endl;
#endif

    };

    void addCones( MappingSimplex &sMapped, MappingSimplex &sOrig,
        IMappingFiltration &cone, int &scale, double &mapTime,
        int &maxDim){

      int nVertices = sMapped.vertices.size() + sOrig.vertices.size();
      bool keepGoing = true;
      if(nVertices <= maxDim + 2 ){
        MappingSimplex sNew = sMapped;
        sNew.vertices.insert( sOrig.vertices.begin(), sOrig.vertices.end() );
        IMappingFiltrationIterator fit = cone.find(sNew);
        keepGoing = fit == cone.end();
        if( keepGoing ){
          cone[sNew] = mapTime;
        }
      }
      if( keepGoing){
        std::list<MappingSimplex> f1 = sMapped.getFaces();
        for(std::list<MappingSimplex>::iterator it = f1.begin(); it != f1.end(); ++it){
          addCones( *it, sOrig, cone, scale, mapTime, maxDim );
        }

        std::list<MappingSimplex> f2 = sOrig.getFaces();
        for(std::list<MappingSimplex>::iterator it = f2.begin(); it != f2.end(); ++it){
          addCones( sMapped, *it, cone, scale, mapTime, maxDim );
        }
      }

    };




};

#endif
