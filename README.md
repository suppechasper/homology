#Multiscale Persistent Homology 

Fast approximate persistence homology computation using a multiscale approach. The multiscale approach 
avoids constructing the full Vietoris-Rips complex by reducing the number of points in a hierrachical fashion. 
At each scale of the haoerarchy the Veitoris-Rips complex is constructed only up to a small distance relative to 
the scale, i.e., on the order of the distance between neighboring points.

![picture](images/image2.png)
![picture](images/image3.png)
![picture](images/image4.png)

The filtrations at each scale are connected by the construction of a mapping cylinder between each subsequent scales.

![picture](images/image9.png)
![picture](images/image5.png)
 
The resulting mutliscale filtration is much smaller than the complete Vietoris-Rips filtration and the persistence diagram of 
the multiscale filtration results in an approximation of the persitence diagram of the Vietoris-Rips filtration on the complete 
point set. No extension or modifications to the standard persistence homology algorithm are required since the multiscale approach 
constructs a valid filtration. Thus, any existing software for the matrix reduction step can be used. The current approach uses the 
very efficient PHAT library. (https://bitbucket.org/phat-code/phat)


The repository contains:

* C++ code implementing multiscale approach to persistence homology
* R package front end
    * Includes code for computing fast Wasserstein distance between diagrams (requires package mop: https://bitbucket.org/suppechasper/optimaltransport )


##R Package Installation

The package can be installed through CMake by running

*  make R_mph_install (after running ccmake)

Prepackaged versions are available from downloads (but might not be the latest versions):

*  https://bitbucket.org/suppechasper/homology/downloads/


###Prerequisites

Requires the gmra package: https://bitbucket.org/suppechasper/gmra

Computing Wasserstein distances requires the mop package: https://bitbucket.org/suppechasper/optimaltransport
 
