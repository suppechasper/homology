#define VERBOSE
#define SAVETOFILE

#include "Precision.h"


#include <stdio.h>

#include <tclap/CmdLine.h>

#include "EigenEuclideanMetric.h"
#include "EigenLinalgIO.h"
#include "IKMTree.h"
#include "NodeDistance.h"

#include "MultiscaleMappingRips.h"
#include "MultiscaleMappingRips2.h"
#include "ConeMapRips.h"
#include "ConeMapRipsPhat.h"





int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("GMRA Rips", ' ', "1");



  TCLAP::ValueArg<std::string> xArg("x","data", "matrix header file", true, "", "input data matrix");
  cmd.add(xArg);

  TCLAP::ValueArg<int> dArg("d","dim", "maxd d-homology to compute", true, 2, "");
  cmd.add(dArg);

  TCLAP::ValueArg<float> pArg("p","percentile", "Radius percentile at each scale", true, 0.5, "");
  cmd.add(pArg);

  TCLAP::SwitchArg sArg("s", "single", "use single scale rips");
  cmd.add(sArg);

  try{
    cmd.parse( argc, argv );
  }
  catch (TCLAP::ArgException &e){
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    return -1;
  }



  int maxD = dArg.getValue();
  bool single = sArg.getValue();
  float percentile = pArg.getValue();

  typedef GMRANode<Precision>::MatrixXp MatrixXp;

  MatrixXp X = EigenLinalg::LinalgIO<Precision>::readMatrix(xArg.getValue());

  MatrixGMRADataObject<Precision> data(X);
  IKMTree<Precision> ikmTree(&data);
  ikmTree.dataFactory = new L2GMRAKmeansDataFactory<Precision>();
  ikmTree.nKids=4;

  std::vector<int> pts(X.cols() );
  for(int i=0; i<pts.size(); i++){
    pts[i] = i;
  };

  ikmTree.addPoints( pts );




  EuclideanMetric<Precision> l2;
  CenterNodeDistance<Precision> dist(&l2);
  ikmTree.computeRadii(&dist);
  ikmTree.computeLocalRadii(&dist);


  GenericGMRANeighborhood<Precision> nh(&ikmTree, &dist);
  ConeMapRips<Precision> cmrips(nh);
  cmrips.run( maxD, percentile, single );

  ConeMapRipsPhat<Precision, phat::row_reduction, phat::vector_heap> cmrips2(nh);
  cmrips2.run( maxD, percentile, single );

  MultiscaleMappingRips<Precision> mrips(nh);
  mrips.run(maxD, percentile, single);

  MultiscaleMappingRips2<Precision> mrips2(nh);
  mrips2.run(maxD, percentile, single);

  return 0;

}
