#ADD_EXECUTABLE(Rips Rips.cxx)
#TARGET_LINK_LIBRARIES (Rips gfortran lapack blas )

ADD_EXECUTABLE(GMRARips GMRARips.cxx)
TARGET_LINK_LIBRARIES (GMRARips)

ADD_EXECUTABLE(Rips Rips.cxx)
TARGET_LINK_LIBRARIES (Rips)

ADD_EXECUTABLE(Grid3dFiltration Grid3dFiltration.cxx)
TARGET_LINK_LIBRARIES (Grid3dFiltration)
