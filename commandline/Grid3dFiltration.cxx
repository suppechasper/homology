#include "Precision.h"

#include <stdio.h>

#include <tclap/CmdLine.h>


#include "Grid3dNeighbors.h"
#include "FiltrationBuilder.h"
#include "PersistentHomology.h"
#include "QuadPersistentHomology.h"

int main(int argc, char **argv){


  clock_t start;
  clock_t end;
  unsigned long millis;

  

  int nx = 11;
  int ny = 11;
  int nz = 11;
  Precision f[nx*ny*nz];
  int mask[nx*ny*nz];
  for(int i =0; i<nx; i++){
    for(int j=0; j<ny; j++){
      for(int k=0; k<nz; k++){
        double r = (5 - i) * (5-i) + (5-j) * (5-j) + (5-k) * (5-k) ;
        double v = 1-exp( -r / 3 );
        f[i*(nz*ny) + j * nz + k] = v;
        mask[ i*(nz*ny) + j * nz + k ] = 1;
      }
    }
  }
  
  

  //Classical filtration
  start = clock();
  
  FiltrationBuilder<Precision>::Neighbors N;
  Grid3dNeighbors<Precision>::build(N, nx, ny, nz, f, mask);  
  
  FiltrationBuilder<Precision> fb;
  fb.run(N, 2);
  Filtration &filt2 = fb.getFiltration();

  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Filtration building time: " << millis << "ms" << std::endl;
  std::cout << "Filtration size: " << filt2.size()  << std::endl;

  start = clock();
  
  PersistentHomology<Precision>  ph;
  ph.run( filt2 );
  
  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Persistence time: " << millis << "ms" << std::endl;
 

  //Quad filtration 
  start = clock();
  
  QuadFiltration filt;
  Grid3dNeighbors<Precision>::buildQuadFiltration(filt, nx, ny, nz, f, mask);
   
  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Quad filtration time: " << millis << "ms" << std::endl;
  std::cout << "Quad filtration size: " << filt.size()  << std::endl;


  start = clock();
  
  QuadPersistentHomology<Precision>  qph;
  qph.run( filt );
  
  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Quad persistence time: " << millis << "ms" << std::endl;


   
  
  return 0;

}
