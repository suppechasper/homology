#include "Precision.h"

#include <stdio.h>

#include <tclap/CmdLine.h>

#include "EigenEuclideanMetric.h"
#include "EigenLinalgIO.h"
#include "IKMTree.h"
#include "NodeDistance.h"

#include "MultiscaleRipsHarer.h"
#include "PersistentHomology.h"






int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("GMRA Rips Harer", ' ', "1");

  TCLAP::ValueArg<std::string> xArg("x","data", "matrix header file", true, "", "input data matrix"); 
  cmd.add(xArg);

  TCLAP::ValueArg<float> pArg("p","percentile", "Radius percentile at each scale", true, 0.5, ""); 
  cmd.add(pArg);


  TCLAP::ValueArg<int> dArg("d","dim", "maxd d-homology to compute", true, 2, ""); 
  cmd.add(dArg);
  
  try{ 
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }


  std::string xFile = xArg.getValue();
  int maxD = dArg.getValue();
  float percentile = pArg.getValue();

  typedef GMRANode<Precision>::MatrixXp MatrixXp;
  MatrixXp X = EigenLinalg::LinalgIO<Precision>::readMatrix(xArg.getValue());

  MatrixGMRADataObject<Precision> data(X);
  IKMTree<Precision> ikmTree(&data);
  ikmTree.dataFactory = new L2GMRAKmeansDataFactory<Precision>(); 
  ikmTree.nKids=4; 

  std::vector<int> pts(X.cols() );
  for(int i=0; i<pts.size(); i++){
    pts[i] = i;
  };

  ikmTree.addPoints( pts );

  EuclideanMetric<Precision> l2;
  CenterNodeDistance<Precision> dist(&l2);
  ikmTree.computeRadii(&dist);
  ikmTree.computeLocalRadii(&dist);


  GenericGMRANeighborhood<Precision> nh(&ikmTree, &dist);

  MultiscaleRipsHarer<Precision> mrips(nh);
  mrips.run(maxD, percentile);
  
  MultiscalePersistentHomology<Precision> persistence;
  persistence.reduce( mrips.getFiltrations(), mrips.getMappings(),
      mrips.getAlphas(), maxD ); 


  return 0;

}
