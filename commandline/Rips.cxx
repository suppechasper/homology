#include "Precision.h"

#include <stdio.h>

#include <tclap/CmdLine.h>

#include "LinalgIO.h"

#include "EuclideanMetric.h"
#include "Distance.h"
#include "FiltrationBuilder.h"
#include "PersistentHomology.h"

int main(int argc, char **argv){

  //Command line parsing
  TCLAP::CmdLine cmd("Rips", ' ', "1");

  TCLAP::ValueArg<std::string> xArg("x","data", "matrix header file", true, "", "input data matrix"); 
  cmd.add(xArg);

  TCLAP::ValueArg<Precision> eArg("e","epsilon", "NN epislon", true, 0, ""); 
  cmd.add(eArg);

  TCLAP::ValueArg<int> kArg("k","knn", "k-NN", true, 100, ""); 
  cmd.add(kArg);

  TCLAP::ValueArg<int> dArg("d","dim", "maxd d-homology to compute", true, 2, ""); 
  cmd.add(dArg);
  
  TCLAP::ValueArg<std::string> oArg("o","out", "out prefix", false, "filt",
      "output prefix for filtrations at each scale"); 
  cmd.add(oArg);

  try{ 
    cmd.parse( argc, argv );
  } 
  catch (TCLAP::ArgException &e){ 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
    return -1;
  }

  clock_t start;
  clock_t end;
  unsigned long millis;

  std::string xFile = xArg.getValue();
  std::string out = oArg.getValue();
  Precision eps = eArg.getValue();
  int knn = kArg.getValue();
  int maxD = dArg.getValue();

  FortranLinalg::DenseMatrix<Precision> X = FortranLinalg::LinalgIO<Precision>::readMatrix(xFile);



  
  start = clock();
  
  FiltrationBuilder<Precision> rips;

  FortranLinalg::DenseMatrix<int> nn(knn, X.N());
  FortranLinalg::DenseMatrix<Precision> nnD(knn, X.N());
  FortranLinalg::EuclideanMetric<Precision> metric;
  FortranLinalg::Distance<Precision>::computeKNN(X, nn, nnD, metric);

  FiltrationBuilder<Precision>::Neighbors N( X.N() );
  for(int i = 0; i<X.N(); i++){
  FiltrationBuilder<Precision>::NeighborMap NN;
    for(int j=0; j<nn.M(); j++){
      NN[ nn(j,i) ] =  nnD(j, i);
    }
    N[i] = NN;
  }
  rips.run(N, maxD);
   
  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Rips time: " << millis << "ms" << std::endl;

  Filtration &filt = rips.getFiltration();

  start = clock();
  
  PersistentHomology<Precision>  ph;
  ph.run( filt );
  
  end = clock();
  millis = (end - start) * 1000 / CLOCKS_PER_SEC;
  std::cout << "Persistence time: " << millis << "ms" << std::endl;
  
  
  
  return 0;

}
